import React, { useEffect } from "react";
import "./admin.css";
//import { toast } from "react-toastify";
// import { logout } from "./function/auth";
// import { useDispatch, useSelector } from "react-redux";
// import { useHistory } from "react-router-dom";

import Routes from "./components/Routes";

import { BrowserRouter, Route } from "react-router-dom";
import Sidebar from "./components/sidebar/Sidebar";
import TopNav from "./components/topNav/TopNav";
import { useDispatch, useSelector } from "react-redux";




const AdminDashBoard = ({ history }) => {
  const { user } = useSelector((state) => ({ ...state }));
  const dispatch = useDispatch();

  const TokenResult = window.localStorage.getItem('token');
  
  
  return (
    <>
     
     
          
            <Route
            render={(props) => (
              <div className={`layout `}>
                <Sidebar {...props} />
               
                <div className="layout__content">
                  <TopNav />
                  <div className="layout__content-main">
                  <Routes/>
                   
                  </div>
                </div>
              </div>
            )}
          />
      
          
        
     
    </>

  );
};

export default AdminDashBoard;
