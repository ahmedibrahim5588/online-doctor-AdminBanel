import React, { useEffect, useState } from "react";
import { Modal, Button, Space, Row, Col } from "antd";
import { addArea, getAllAreas, deleteArea } from "../../function/area";
import { toast } from "react-toastify";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";

const Area = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [nameAr, setnameAr] = useState("");
  const [nameEn, setnameEn] = useState("");
  const [areas, setareas] = useState([]);
  const { user } = useSelector((state) => ({ ...state }));

  useEffect(() => {
    allcities();
  }, []);

  const formdate = (date) => {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [year, month, day].join("-");
  };

  const allcities = () => {
    getAllAreas().then((res) => {
      console.log("from area comp", res.data.data);
      setareas(res.data.data);
    });
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = (e) => {
    e.preventDefault();
    console.log(nameAr, nameEn);
    // addCity(nameAr, nameEn)
    //   .then((res) => {
    //     console.log("res in citycomp", res);
    //     setnameAr("");
    //     setnameEn("");
    //     toast.success(`new City is created`);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     // console.log(e.response.data.errors[0].msg);
    //     toast.warning(err.response.data.errors[0].msg);
    //   });

    // setIsModalVisible(false);
    // allcities();
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const handleRemove = (id) => {
    if (window.confirm(`Delete? ${id}`)) {
      //setloading(true);
      // deleteCity(id, `bearer ${user.token}`)
      //   .then((res) => {
      //     console.log("city is deleted");
      //     toast.error(`city with id ${id} is deleted`);
      //     allcities();
      //   })
      //   .catch((err) => {
      //     toast.error(err);
      //   });
    }
  };
  const categoryForm = () => (
    <div className="container-fluid ">
      <form>
        <div className="form-group">
          <label>nameENg</label>
          <input
            type="text"
            className="form-control"
            placeholder="nameEn"
            s
            name="nameEn"
            value={nameEn}
            onChange={(e) => setnameEn(e.target.value)}
            autoFocus
            required
          />
        </div>
        <div className="form-group">
          <label>nameAr</label>
          <input
            type="text"
            className="form-control"
            placeholder="nameAr"
            name="nameAr"
            value={nameAr}
            onChange={(e) => setnameAr(e.target.value)}
            autoFocus
            required
          />
        </div>
      </form>
    </div>
  );
  return (
    <>
      <Row>
        <Col span={8}>
          <Button
            type="primary"
            align="center"
            className="text-center"
            onClick={showModal}
          >
            Create New Area
          </Button>
        </Col>
      </Row>

      {/* {JSON.stringify(cities)} */}
      <Modal
        title="City Modal"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        {categoryForm()}
      </Modal>

      <hr />

      <table class="table table-striped text-center">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Area in Arabic</th>
            <th scope="col">Area in english</th>

            <th scope="col">createdAt</th>
            <th scope="col">city</th>
            <th scope="col" className="text-center">
              control
            </th>
          </tr>
        </thead>
        <tbody>
          {areas.map((c) => (
            <tr className="alert alert-primary" key={c.id}>
              <th scope="row" key={c.id}>
                {c.id}
              </th>
              <td>{c.name.ar}</td>
              <td>{c.name.en}</td>
              <td>{formdate(c.createdAt)}</td>
              <td>{c.city}</td>
              <td>
                <span
                  onClick={() => handleRemove(c.id)}
                  className="btn btn-sm float-right"
                >
                  <DeleteOutlined className="text-danger" />
                </span>

                <span
                  // onClick={() => handleUpdate(c.id)}
                  className="btn btn-sm float-left"
                >
                  <EditOutlined className="text-warning" />
                </span>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default Area;
