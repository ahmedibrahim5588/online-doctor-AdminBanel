import React from "react";
import { Tabs } from "antd";
import City from "./City";
import Area from "./Area";
const { TabPane } = Tabs;

const Location = () => {
  return (
    <Tabs defaultActiveKey="1" centered>
      <TabPane tab="المحافظات " key="1">
        <City />
      </TabPane>
      <TabPane tab="المناطق" key="2">
        <Area />
      </TabPane>
    </Tabs>
  );
};

export default Location;
