import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import {
  addCategory,
  getAllCategories,
  deleteCategory,
} from "../../function/category";

import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import UploadFile from "../../components/uploadFile/UploadFile";

const CreateCategory = () => {
  const { user } = useSelector((state) => ({ ...state }));

  const [name, setName] = useState({});
  const [nameEn, setnameEn] = useState("");
  const [nameAr, setnameAr] = useState("");
  const [img, setimg] = useState(
    "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
  );
  const [loading, setloading] = useState(false);
  const [categories, setCategories] = useState([]);
  const history = useHistory();

  useEffect(() => {
    loadCategories();
  }, []);

  const loadCategories = () => {
    getAllCategories().then((c) => {
      setCategories(c.data.data);
      // setCategories(c.data);
    });
  };

  const handleRemove = (id) => {
    if (window.confirm("Delete?")) {
      setloading(true);
      deleteCategory(id)
        .then(() => {
          setloading(false);
          toast.error(`category with id ${id} is deleted`);
          loadCategories();
        })
        .catch((err) => {
          if (err.response.status === 400) {
            setloading(false);
            toast.error(err.response.data);
          }
        });
    }
  };

  const handleFile = (e) => {
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setimg(reader.result);
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const handlesubmet = (e) => {
    e.preventDefault();
    let formData = new FormData();
    formData.append("nameAr", nameAr);
    formData.append("nameEn", nameEn);
    formData.append("img", img);
    // setloading(true);

    // console.log(name);
    addCategory(formData)
      .then((res) => {
        console.log(res);
        setloading(false);
        setnameAr("");
        setnameEn("");
        setimg(
          "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
        );
        toast.success(`new category is created`);
        loadCategories();
      })
      .catch((err) => {
        console.log(err);
        // console.log(e.response.data.errors[0].msg);
        toast.warning(err);
      });
  };
  const handleUpdate = (id) => {
    history.push(`/admin/categories/${id}`);
  };
  const categoryForm = () => (
    <div className="container-fluid ">
      <form onSubmit={handlesubmet}>
        <div className="form-group">
          <label>nameENg</label>
          <input
            type="text"
            className="form-control"
            placeholder="nameEn"
            s
            name="nameEn"
            value={nameEn}
            onChange={(e) => setnameEn(e.target.value)}
            autoFocus
            required
          />
        </div>
        <div className="form-group">
          <label>nameAr</label>
          <input
            type="text"
            className="form-control"
            placeholder="nameAr"
            name="nameAr"
            value={nameAr}
            onChange={(e) => setnameAr(e.target.value)}
            autoFocus
            required
          />
        </div>

        <div className="container-fluid">
          <div className="row">
            <div className="container-fluid">
              <UploadFile
                profileImg={img}
                fileSelectedHandler={handleFile}
                title="Category Image"
              />
            </div>
          </div>
          <button
            type="submit"
            className="btn btn-outline-primary mt-3 btn-lg btn-block"
          >
            Save
          </button>
        </div>
      </form>
    </div>
  );

  return (
    <div className="container-fluid">
      <di className="row">
        <div className="col-md-12">
          {loading ? (
            <h4 className="text-danger"> loading ... </h4>
          ) : (
            <h4 className="text-primary">create Category</h4>
          )}
          {categoryForm()}
          <hr />

          <table class="table table-striped text-center">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Name in Arabic</th>
                <th scope="col">Name in english</th>
                <th scope="col">createdAt</th>
              </tr>
            </thead>
            <tbody>
              {categories.map((c) => (
                <tr className="alert alert-primary" key={c.id}>
                  <th scope="row" key={c.id}>
                    {c.id}
                  </th>
                  <td>{c.name.ar}</td>
                  <td>{c.name.en}</td>
                  <td>{c.createdAt}</td>

                  <span
                    onClick={() => handleRemove(c.id)}
                    className="btn btn-sm float-right"
                  >
                    <DeleteOutlined className="text-danger" />
                  </span>

                  <span
                    onClick={() => handleUpdate(c.id)}
                    className="btn btn-sm float-left"
                  >
                    <EditOutlined className="text-warning" />
                  </span>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </di>
    </div>
  );
};

export default CreateCategory;
