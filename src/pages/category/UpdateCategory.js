import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import { useParams } from "react-router";
import { getCategory, updateCategory } from "../../function/category";
import UploadFile from "../../components/uploadFile/UploadFile";

const UpdateCategory = ({ history, match }) => {
  const { user } = useSelector((state) => ({ ...state }));

  const [nameEn, setnameEn] = useState("");
  const [nameAr, setnameAr] = useState("");
  const [file, setFile] = useState("");
  const [loading, setloading] = useState(false);
  const [img, setimg] = useState(
    "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
  );
  useEffect(() => {
    loadCategory();
  }, []);

  const loadCategory = () => {
    getCategory(match.params.id).then((res) => {
      console.log(res.data);
      setnameAr(res.data.name.ar);
      setnameEn(res.data.name.en);
      setimg(res.data.img);
    });
  };

  const handleFile = (e) => {
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setimg(reader.result);
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const handlesubmet = (e) => {
    e.preventDefault();
    let formData = new FormData();
    formData.append("nameAr", nameAr);
    formData.append("nameEn", nameEn);
    formData.append("img", img);
    // setloading(true);

    // console.log(name);
    updateCategory(match.params.id, formData)
      .then((res) => {
        console.log(res);
        setloading(false);
        setnameAr("");
        setnameEn("");
        toast.success(` category with id ${match.params.id} is updated`);
        history.push("/admin/categories");
      })
      .catch((err) => {
        console.log(err);
        // console.log(e.response.data.errors[0].msg);
        toast.warning(err);
      });
  };

  const categoryForm = () => (
    <div className="container-fluid ">
      <form onSubmit={handlesubmet}>
        <div className="form-group">
          <label>nameENg</label>
          <input
            type="text"
            className="form-control"
            placeholder="nameEn"
            s
            name="nameEn"
            value={nameEn}
            onChange={(e) => setnameEn(e.target.value)}
            autoFocus
            required
          />
        </div>
        <div className="form-group">
          <label>nameAr</label>
          <input
            type="text"
            className="form-control"
            placeholder="nameAr"
            name="nameAr"
            value={nameAr}
            onChange={(e) => setnameAr(e.target.value)}
            autoFocus
            required
          />
        </div>

        <div className="container-fluid">
          <div className="row">
            <div className="container-fluid">
              <UploadFile
                profileImg={img}
                fileSelectedHandler={handleFile}
                title="Category Image"
              />
            </div>
          </div>
          <button
            type="submit"
            className="btn btn-outline-primary mt-3 btn-lg btn-block"
          >
            Save
          </button>
        </div>
      </form>
    </div>
  );

  return (
    <div className="container-fluid">
      <di className="row">
        <div className="col-md-12">
          {loading ? (
            <h4 className="text-danger"> loading ... </h4>
          ) : (
            <h4 className="text-primary"> update Category</h4>
          )}
          {categoryForm()}
        </div>
      </di>
    </div>
  );
};

export default UpdateCategory;
