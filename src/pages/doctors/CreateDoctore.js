import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { useSelector } from "react-redux";
import {
  getAllDoctors,
  acceptDoctor,
  getdoctor,
  addDoctor,
  deleteDoctor,
} from "../../function/doctore";

import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import Password from "antd/lib/input/Password";
import { Input } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";
import "./cretedoctor.css";
import UploadFile from "../../components/uploadFile/UploadFile";
import { Switch } from "antd";
import axios from "axios";
import TogleButton from "../../components/button/TogleButton";
const CreateDoctore = () => {
  const { user } = useSelector((state) => ({ ...state }));

  const [type, setType] = useState("DOCTOR");

  const [gender, setgender] = useState("");
  const [accepted, setaccepted] = useState("false");
  const [addresses, setaddresses] = useState([]);
  const [chatCost, setchatCost] = useState(0);
  const [cvPhotos, setcvPhotos] = useState([]);
  const [deleted, setdeleted] = useState([]);
  const [kind, setkind] = useState("");
  const [numberOfClientRating, setnumberOfClientRating] = useState(0);
  const [numberOfOrders, setnumberOfOrders] = useState(0);
  const [rating, setrating] = useState(0);
  const [signupType, setsignupType] = useState("");
  const [stopNotification, setstopNotification] = useState(false);

  const [title, settitle] = useState("doctor");
  const [verified, setverified] = useState(false);
  const [videoCost, setvideoCost] = useState(false);

  const [aboutDr, setaboutDr] = useState("");
  const [email, setemail] = useState("");
  const [phone, setphone] = useState("");
  const [loading, setloading] = useState(false);
  const [username, setusername] = useState("");
  const [doctors, setdoctors] = useState([]);
  const [password, setpassword] = useState("");

  const [toggle, setTogle] = useState(false);
  const [idPhotofront, setidPhotofront] = useState(
    "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
  );
  const [idPhotoback, setidPhotoback] = useState(
    "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
  );
  const [cardPhotofront, setcardPhotofront] = useState(
    "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
  );
  const [cardPhotoback, setcardPhotoback] = useState(
    "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
  );
  const [certificate, setcertificate] = useState(
    "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
  );

  const [idPhotos, setIdPhotos] = useState([idPhotofront, idPhotoback]);
  const [professionPhotos, setprofessionPhotos] = useState([
    cardPhotoback,
    cardPhotofront,
  ]);
  const [syndicatePhotos, setsyndicatePhotos] = useState([certificate]);
  const history = useHistory();

  useEffect(() => {
    LoadDoctors();
    // handleaccepted();
  }, []);
  const handlesubmet = (e) => {
    e.preventDefault();

    let formData = new FormData();

    formData.append("idPhotos", idPhotos);
    formData.append("professionPhotos", professionPhotos);
    formData.append("syndicatePhotos", syndicatePhotos);

    addDoctor(formData, username, email, type, password, phone, aboutDr, title)
      .then((res) => {
        console.log("create doctor sucess", res);
        setloading(false);
        setaboutDr("");
        setemail("");
        setphone("");
        setusername("");
        setpassword("");
        settitle("");
        setidPhotofront(
          "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
        );
        setidPhotoback(
          "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
        );
        setcardPhotofront(
          "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
        );
        setcardPhotoback(
          "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
        );
        setcertificate(
          "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png"
        );
        toast.success(`new Doctor is created`);
        // loadCategories();
      })
      .catch((err) => {
        console.log(err);
        // console.log(e.response.data.errors[0].msg);
        toast.warning(err.response.data.errors[0].msg);
      });
  };
  const LoadDoctors = () => {
    getAllDoctors().then((d) => {
      console.log(d.data.data);
      setdoctors(d.data.data);
    });
  };

  const handleRemove = (id) => {
    if (window.confirm("Delete?")) {
      setloading(true);
      deleteDoctor(id, `bearer ${user.token}`)
        .then(() => {
          setloading(false);
          toast.error(`doctor with id ${id} is deleted`);
          LoadDoctors();
        })
        .catch((err) => {
          if (err.response.status === 400) {
            setloading(false);
            toast.error(err.response.data);
          }
        });
    }
  };

  const fileSelectedHandler = (e) => {
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setidPhotofront(reader.result);
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const fileSelectedpfotobackHandler = (e) => {
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setidPhotoback(reader.result);
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const fileSelectedpcardfrontHandler = (e) => {
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setcardPhotofront(reader.result);
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const fileSelectedpcardbackHandler = (e) => {
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setcardPhotoback(reader.result);
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  };
  const fileSelectedcertificate = (e) => {
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setcertificate(reader.result);
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  };

  const handleaccepted = (id, accept) => {
    getdoctor(id).then((res) => {
      console.log(`from userbid ${id} befour`, res.data.accepted);
      setaccepted(`from userbid ${id} befour`, res.data.accepted);
    });
    acceptDoctor(id);
    getdoctor(id).then((res) => {
      setaccepted(res.data.accepted);
      console.log(`after accepted ${id}`, res.data.accepted);

      // console.log("after accept", res.data.accepted)
    });
  };
  const gettoglestatuswithacceptstatus = (accept) => {
    if (accept === true) {
      setTogle(true);
    }
    setTogle(false);
  };
  useEffect(() => {
    gettoglestatuswithacceptstatus();
  });
  // getAllDoctors();

  const DoctorForm = () => (
    <div class="row ">
      <div className="col-lg-12 mx-auto">
        <div className="card mt-2 mx-auto p-4 bg-light">
          <div className="card-body bg-light">
            <div className="container-fluid ">
              <form id="contact-form" onSubmit={handlesubmet} role="form">
                <div className="controls">
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        {" "}
                        <label for="form_name">Doctor Name</label>
                        <input
                          id="form_name"
                          type="text"
                          name="username"
                          value={username}
                          onChange={(e) => setusername(e.target.value)}
                          className="form-control"
                          placeholder="Please enter Doctor name *"
                          required="required"
                          data-error="Doctor name is required."
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        {" "}
                        <label for="form_lastname">Email</label>{" "}
                        <input
                          id="form_Email"
                          type="email"
                          name="Email"
                          value={email}
                          onChange={(e) => setemail(e.target.value)}
                          className="form-control"
                          placeholder="Please enter Dr email *"
                          required="required"
                          data-error="email is required."
                        />{" "}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <Input.Password
                          name="password"
                          value={password}
                          onChange={(e) => setpassword(e.target.value)}
                          className="form-control"
                          placeholder="please input doctor password"
                          required="required"
                          data-error="password  is required."
                          iconRender={(visible) =>
                            visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                          }
                        />
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        {" "}
                        <label for="form_need">telephone number *</label>{" "}
                        <input
                          type="tel"
                          id="form_tel"
                          name="phone"
                          value={phone}
                          onChange={(e) => setphone(e.target.value)}
                          className="form-control"
                          placeholder="Please enter Dr phone *"
                          required="required"
                          data-error="phone is required."
                        />
                        {/* <select
                          id="form_need"
                          name="Gender"
                          value={gender}
                          className="form-control mt-3"
                          required="required"
                          data-error="Please specify your need."
                        >
                          <option onChange={(e) => setgender(e.target.value)}>
                            Male
                          </option>
                          <option onChange={(e) => setgender(e.target.value)}>
                            Female
                          </option>
                        </select>{" "} */}
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="form-group">
                        {" "}
                        <label for="form_message">aboutDr *</label>{" "}
                        <textarea
                          id="form_message"
                          name="aboutDr"
                          value={aboutDr}
                          onChange={(e) => setaboutDr(e.target.value)}
                          className="form-control"
                          placeholder="Please, leave us an introduction"
                          rows="4"
                          required="required"
                          data-error="Please, leave us an introduction "
                        ></textarea>{" "}
                      </div>
                    </div>

                    <div className="container-fluid">
                      <div className="row">
                        <div className="col-md-6">
                          <UploadFile
                            profileImg={idPhotofront}
                            fileSelectedHandler={fileSelectedHandler}
                            title="البطاقه الشخصيه من الامام"
                          />
                        </div>

                        <div className="col-md-6">
                          <UploadFile
                            profileImg={idPhotoback}
                            fileSelectedHandler={fileSelectedpfotobackHandler}
                            title="البطاقه الشخصيه من الخلف"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <UploadFile
                        profileImg={cardPhotofront}
                        fileSelectedHandler={fileSelectedpcardfrontHandler}
                        title="صوره كارنيه النقابه من الامام"
                      />
                    </div>

                    <div className="col-md-6">
                      <UploadFile
                        profileImg={cardPhotoback}
                        fileSelectedHandler={fileSelectedpcardbackHandler}
                        title="صوره كارنيه النقابه من الخلف"
                      />
                    </div>

                    <div className="col-md-12">
                      <UploadFile
                        profileImg={certificate}
                        fileSelectedHandler={fileSelectedcertificate}
                        title="شهاده مزاوله الخدمه"
                      />
                    </div>

                    <div className="col-md-12">
                      <input
                        type="submit"
                        className="btn btn-success btn-send pt-2 btn-block "
                        value="Crate Doctor"
                      />
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div classNameName="container-fluid">
      <di className="row">
        <div className="col-md-12">
          {loading ? (
            <h4 className="text-danger"> loading ... </h4>
          ) : (
            <h4 className="text-primary">create New Doctor</h4>
          )}
          {DoctorForm()}
          <hr />

          <table class="table table-striped text-center">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Name </th>
                <th scope="col">Email</th>
                <th scope="col">Telephone</th>
                <th scope="col">Gender</th>
                <th scope="col">accepted</th>
                <th scope="col">verified</th>
                <th scope="col">
                  Change accepted status <br />
                  <sub> change the button to reversr accepted status</sub>
                </th>
              </tr>
            </thead>
            <tbody>
              {doctors.map((d) => (
                <tr className="alert alert-primary" key={d.id}>
                  <th scope="row">{d.id}</th>

                  <td>{d.username}</td>
                  <td>{d.email}</td>
                  <td>{d.phone}</td>
                  <td>{d.gender}</td>
                  {d.accepted === true ? (
                    <td>
                      <span id="accepted" className="badge bg-success">
                        yes
                        {/* <Switch
                          onClick={() => handleaccepted(d.id, d.accepted)}
                          // checkedChildren="YES"
                          // unCheckedChildren="NO"
                        /> */}
                      </span>
                    </td>
                  ) : (
                    <td>
                      <span id="accepted" className="badge bg-danger">
                        NO
                        {/* <Switch
                          onClick={() => handleaccepted(d.id, d.accepted)}
                          // checkedChildren="YES"
                          // // unCheckedChildren="NO"
                        /> */}
                      </span>
                    </td>
                  )}

                  {d.verified === true ? (
                    <td>
                      <span className="badge bg-success">YES</span>
                    </td>
                  ) : (
                    <td>
                      <span className="badge bg-danger">NO</span>
                    </td>
                  )}
                  <td>
                    <Switch
                      onClick={() => handleaccepted(d.id, d.accepted)}
                      // checkedChildren="YES"
                      // unCheckedChildren="NO"
                    />
                  </td>
                  <span
                    onClick={() => handleRemove(d.id)}
                    className="btn btn-sm text-center "
                  >
                    <DeleteOutlined className="text-danger" />
                  </span>
                  {/* <span
                    //   onClick={() => handleRemove(c.id)}
                    className="btn btn-sm float-right"
                  >
                    <DeleteOutlined className="text-danger" />
                  </span>

                  <span
                    //onClick={() => handleUpdate(c.id)}
                    className="btn btn-sm float-left"
                  >
                    <EditOutlined className="text-warning" />
                  </span> */}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </di>
    </div>
  );
};

export default CreateDoctore;
