import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Switch, Route } from "react-router-dom";

import "react-toastify/dist/ReactToastify.css";

import Customers from "./pages/Customers";
import Dashboard from "./pages/Dashboard";
import Products from "./pages/Products";
import AdminDashBoard from "./AdminDashBoard";
import Login from "./auth/Login";
// import Logout from "./auth/Logout";
// import { getUser } from "./function/auth";
import NoMatch from "./pages/NoMatch";
import { getUser } from "./function/auth";

const App = () => {
  // const { user } = useSelector((state) => ({ ...state }));

  const dispatch = useDispatch();
  useEffect(() => {
    const TokenResult = window.localStorage.getItem("token");
    const userId = window.localStorage.getItem("_id");
    if (TokenResult && userId) {
      getUser(userId, `bearer ${TokenResult}`)
        .then((res) => {
          console.log("res from app", res);
          dispatch({
            type: "USERAUTH",
            payload: {
              accepted: res.data.user.accepted,
              createdAt: res.data.user.createdAt,
              deleted: res.data.user.deleted,
              username: res.data.user.username,
              email: res.data.user.email,
              token: TokenResult,
              kind: res.data.user.kind,
              numberOfOrders: res.data.user.numberOfOrders,
              phone: res.data.user.phone,
              rules: res.data.user.rules,
              signupType: res.data.user.signupType,
              stopNotification: res.data.user.stopNotification,
              verified: res.data.user.verified,
              type: res.data.user.type,
              id: res.data.user.id,
            },
          });
        })
        .catch((err) => console.log(err));
    } else {
      window.localStorage.removeItem("token");
      window.localStorage.removeItem("_id");
      dispatch({
        type: "NOTAUTH",
        payload: null,
      });
    }
  }, [dispatch]);

  return (
    <>
      <Route exact path="/" component={Login} />

      <Switch>
        <Route path="/admin">
          <AdminDashBoard />
        </Route>
      </Switch>
    </>
  );
};

export default App;
