import React from "react";

import { Route, Switch } from "react-router-dom";
import CreateCategory from "../pages/category/CreateCategory";
import UpdateCategory from "../pages/category/UpdateCategory";
import Dashboard from "../pages/Dashboard";
import CreateDoctore from "../pages/doctors/CreateDoctore";
import Location from "../pages/location/Location";

const Routes = () => {
  return (
    <Switch>
      <Route path="/admin/dashboard" exact component={Dashboard} />
      <Route path="/admin/categories" exact component={CreateCategory} />
      <Route path="/admin/categories/:id" exact component={UpdateCategory} />
      <Route path="/admin/doctors" exact component={CreateDoctore} />
      <Route path="/admin/location" exact component={Location} />
    </Switch>
  );
};

export default Routes;
