import React from "react";

const UploadFile = ({ profileImg, fileSelectedHandler, title }) => {
  return (
    <div className=" mb-3 ml-5">
      <div className="container">
        <h4 className="heading text-center">{title}</h4>
        <div className="img-holder">
          <img src={profileImg} alt="" id="img" className="img" />
        </div>

        <label for="file-upload" class="custom-file-upload">
          upload file
        </label>
        <input
          type="file"
          className="upload text-center"
          onChange={fileSelectedHandler}
          name="img"
        />
        {/* <button onClick={fileUploadHandler}>Upload</button> */}
      </div>
    </div>
  );
};

export default UploadFile;
