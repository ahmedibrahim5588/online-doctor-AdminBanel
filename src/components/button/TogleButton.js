import React, { useEffect, useState } from "react";

class TogleButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonState: this.props.accepted,
    };
    this.toggleState = this.toggleState.bind(this);
  }

  render() {
    return (
      <div>
        <h2>Button Toggle: {this.state.buttonState.toString()}</h2>
        <button onClick={this.toggleState}>Toggle State</button>
      </div>
    );
  }

  toggleState() {
    this.setState({ buttonState: !this.state.buttonState });
  }
}
export default TogleButton;
