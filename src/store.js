import { createStore, applyMiddleware } from "redux";

import { composeWithDevTools } from "redux-devtools-extension";

import thunk from "redux-thunk";

import rootReudcer from "./reducers/index";

const initialState = {};

const middleware = [thunk];

const store = createStore(
  rootReudcer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
