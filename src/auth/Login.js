import React, { useEffect, useState } from "react";
import "./login.css";
import image from "./images/undraw_remotely_2j6y.svg";
import logo from "../assets/images/logo.jpeg";
import { login } from "../function/auth";
import { Button } from "antd";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";

const Login = ({ history }) => {
  const [email, setEmail] = useState("admin@admin.com");
  const [password, setPassword] = useState("aaaaaa");
  const [type] = useState("ADMIN");
  const { user } = useSelector((state) => ({ ...state }));

  let dispatch = useDispatch();

  const roleBasedRedirect = (res) => {
    if (res.data.user.type === "ADMIN") {
      history.push("/admin");
    } else if (res.data.user.type !== "ADMIN" && !res.data.token) {
      toast.error("NOOOOOO");
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    window.localStorage.removeItem("token");
    window.localStorage.removeItem("_id");
    try {
      login(email, password, type)
        .then((res) => {
          console.log("loged in sucess", res);
          // window.localStorage.removeItem("token");
          // dispatch({
          //   type: "LOGOUT",
          //   payload: null,
          // });

          window.localStorage.setItem("token", res.data.token);

          window.localStorage.setItem("_id", res.data.user.id);

          const TokenResult = window.localStorage.getItem("token");
          const UserId = window.localStorage.getItem("_id");
          dispatch({
            type: "LOGGED_IN_USER",
            payload: {
              accepted: res.data.user.accepted,
              createdAt: res.data.user.createdAt,
              deleted: res.data.user.deleted,
              username: res.data.user.username,
              email: res.data.user.email,
              token: TokenResult,
              kind: res.data.user.kind,
              numberOfOrders: res.data.user.numberOfOrders,
              phone: res.data.user.phone,
              rules: res.data.user.rules,
              signupType: res.data.user.signupType,
              stopNotification: res.data.user.stopNotification,
              verified: res.data.user.verified,
              type: res.data.user.type,
              id: UserId,
            },
          });
          roleBasedRedirect(res);
          toast.success(
            "login sucess and you are directing to admin dash board"
          );
          history.push("/admin");
        })
        .catch((err) => toast.error("NOOOOOO"));
    } catch (error) {
      console.log(error);
      toast.error(error.message);
    }
  };

  const loginForm = () => (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <input
          type="email"
          className="form-control my-3"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="Your email"
          autoFocus
        />
      </div>

      <div className="form-group">
        <input
          type="password"
          className="form-control"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          placeholder="Your password"
        />
      </div>

      <br />
      <Button
        onClick={handleSubmit}
        type="primary"
        className="mb-3"
        block
        shape="round"
        size="large"
        disabled={!email || password.length < 6}
      >
        Login with Email/Password
      </Button>
    </form>
  );

  return (
    <>
      <div className="content">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <img src={image} alt="logo" className="img-fluid mt-9" />
            </div>
            <div className="col-md-6 contents">
              <div className="row justify-content-center">
                <div className="col-md-8">
                  <div className="mb-4">
                    <img src={logo} alt="logo" className="img-fluid" />

                    <h3 className="mt-3">Sign In</h3>
                    <p className="mb-4">
                      Lorem ipsum dolor sit amet elit. Sapiente sit aut eos
                      consectetur adipisicing.
                    </p>
                  </div>
                  {loginForm()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <Layout>
        <Content>
          <Row>
            <Col className="p-3 mb-2 bg-primary text-white" span={16}>
              col-18 col-push-6
            </Col>
            <Col className="p-3 mb-2 bg-success text-white" span={8}>
              col-6 col-pull-18
            </Col>
          </Row>
          ,
        </Content>
      </Layout> */}
    </>
  );
};

export default Login;
