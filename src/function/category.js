import axios from "axios";

export const getAllCategories = async () =>
  await axios.get(`https://onlinedoc-24.com/api/categories`);

export const addCategory = async (formData, img) => {
  await axios.post(`https://onlinedoc-24.com/api/categories`, formData, {
    headers: {
      "content-type": "multipart/form-data",
    },
  });
};
export const getCategory = async (categoryId) =>
  await axios.get(
    `https://onlinedoc-24.com/api/categories/${categoryId}`
    //    {
    //     headers: {
    //       Authorization,
    //     },
    //   }
  );

export const updateCategory = async (categoryId, formData) =>
  await axios.put(
    `https://onlinedoc-24.com/api/categories/${categoryId}`,
    formData

    //    {
    //     headers: {
    //       Authorization,
    //     },
    //   }
  );

export const deleteCategory = async (categoryId) =>
  await axios.delete(
    `https://onlinedoc-24.com/api/categories/${categoryId}`
    //    {
    //     headers: {
    //       Authorization,
    //     },
    //   }
  );
