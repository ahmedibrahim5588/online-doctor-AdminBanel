import axios from "axios";

export const getAllAreas = async () =>
  await axios.get(`https://onlinedoc-24.com/api/areas`);

export const addArea = async (nameAr, nameEn) => {
  await axios.post(
    `https://onlinedoc-24.com/api/areas`,
    {
      nameAr,
      nameEn,
    },
    {
      headers: {
        "content-type": "application/json",
      },
    }
  );
};

// export const acceptDoctor = async (doctorId) =>
//   await axios.put(
//     `https://onlinedoc-24.com/api/users/${doctorId}/accept-account`

//     //    {
//     //     headers: {
//     //       Authorization,
//     //     },
//     //   }
//   );
// export const getdoctor = async (doctorId) =>
//   await axios.get(
//     `https://onlinedoc-24.com/api/doctors/${doctorId}`
//     //    {
//     //     headers: {
//     //       Authorization,
//     //     },
//     //   }
//   );

export const deleteArea = async (areaid, Authorization) =>
  await axios.delete(`https://onlinedoc-24.com/api/areas/${areaid}`, {
    headers: {
      Authorization,
    },
  });
