import axios from "axios";

export const login = async (email, password, type) => {
  return await axios.post(`https://onlinedoc-24.com/api/users/login`, {
    email,
    password,
    type,
  });
};

export const logout = async (authtoken) => {
  return await axios.post(
    `https://onlinedoc-24.com/api/users/logout`,

    {},
    // {
    //   headers: {
    //     authtoken,
    //   },
    // }
  );
};
export const getUser = async (_id,Authorization) =>
  await axios.get(`https://onlinedoc-24.com/api/users/${_id}`,{
    headers: {
      Authorization
    }});
