import axios from "axios";

export const getAllDoctors = async () =>
  await axios.get(`https://onlinedoc-24.com/api/doctors`);

export const addDoctor = async (
  formData,
  username,
  email,
  type,
  password,
  phone,
  aboutDr,
  title
) => {
  await axios.post(
    `https://onlinedoc-24.com/api/users/signup`,
    {
      formData,
      username,
      email,
      type,
      password,
      phone,
      aboutDr,
      title,
    },
    {
      headers: {
        "content-type": "application/json",
      },
    }
  );
};

export const acceptDoctor = async (doctorId) =>
  await axios.put(
    `https://onlinedoc-24.com/api/users/${doctorId}/accept-account`

    //    {
    //     headers: {
    //       Authorization,
    //     },
    //   }
  );
export const getdoctor = async (doctorId) =>
  await axios.get(
    `https://onlinedoc-24.com/api/doctors/${doctorId}`
    //    {
    //     headers: {
    //       Authorization,
    //     },
    //   }
  );

export const deleteDoctor = async (doctorid, Authorization) =>
  await axios.delete(`https://onlinedoc-24.com/api/doctors/${doctorid}`, {
    headers: {
      Authorization,
    },
  });
