import axios from "axios";

export const getAllCities = async () =>
  await axios.get(`https://onlinedoc-24.com/api/cities`);

export const addCity = async (nameAr, nameEn) => {
  await axios.post(
    `https://onlinedoc-24.com/api/cities`,
    {
      nameAr,
      nameEn,
    },
    {
      headers: {
        "content-type": "application/json",
      },
    }
  );
};

// export const getdoctor = async (doctorId) =>
//   await axios.get(
//     `https://onlinedoc-24.com/api/doctors/${doctorId}`
//     //    {
//     //     headers: {
//     //       Authorization,
//     //     },
//     //   }
//   );

export const deleteCity = async (cityId, Authorization) =>
  await axios.delete(`https://onlinedoc-24.com/api/cities/${cityId}`, {
    headers: {
      Authorization,
    },
  });
